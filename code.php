<?

    @session_start();

    class core{
        public static $DBH;
        public static $lang;

        public static function setup(){

            try{
                $dsn = "mysql:host=$host;dbname=$dbname";
                $opt = [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
                ];
            } catch (PDOException $e) {
                echo $e->getMessage();
            }
			
			include_once("lang.php");

            self::$DBH = new PDO($dsn, $user , $pass, $opt);
			self::$lang = $lang;
			return self::$DBH;
        }

        public static function serializeAray($array){
            # code...
            $arr = [];
            foreach ($array as $key => $value) {
                # code...
                $arr[$key] = htmlspecialchars($value);
            }
            return $arr;
        }

        public static function getIP(){
            # code...
            $ip = 0;
            if (!empty($_SERVER['HTTP_CLIENT_IP'])){
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }	

    }