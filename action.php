<?
	@session_start();
	
	class action extends Project{
		
		public static function saveImg($files, $typeImg = null){
			try{
				$err = []; # массив ошибок
				$size = 2200000; # допустимый размер файла в байтах
				$name = time().rand(1000, 10000);
				$destination = '../upload/' . $name;
				
				if ($files){
					$gd = getimagesize($files['tmp_name']); # данные о файле
					$whiteList = ["image/png", "image/jpg", "image/jpeg", "image/gif"]; # белый список типов
					$type = explode("/", $gd['mime']); # берём тип файла
					$valid_file_extensions = [".jpg", ".jpeg", ".gif", ".png"];
					$file_extension = strrchr($files["name"], ".");
					
					if (!in_array(strtolower($files['type']), $whiteList) || !in_array($gd['mime'], $whiteList))
						$err[] = 'Неверный тип файла';
					
					if (!in_array(strtolower($file_extension), $valid_file_extensions) || @getimagesize($files["tmp_name"]) == false)
						$err[] = 'Плохой файл';
					/*
					if (!preg_match("/^(https://".$_SERVER["HTTP_HOST"]."/)(.*?)+$/", $_SERVER["HTTP_REFERER"]))
						$err[] = 'Domain mismatch';
					*/
					if ($files['size'] > $size)
						$err[] = 'Размер файла слишком большой';

					/*if (disk_free_space("/") < 4096000)
						$err[] = 'Ошибка. Повторите позднее.';*/

					if (!$err){
						if ($typeImg == 'banner')
							$file = $destination . '.gif';
						else 
							$file = $destination . '.jpg';
							
						if (file_exists($file)){
							//chmod('/backoffice/uploads/filess' . $file, 0755);
							unlink($file); //Удаление
						}
						if (move_uploaded_file($files['tmp_name'], $file)){ 
							if ($typeImg == 'banner')
								return $name.'.gif';
							else 
								return $name.'.jpg';
						}
					} else {
						print_r($err);
					}
				}
				return false;
			} catch (Exception $e){
				parent::logError('Выброшено исключение: '.  $e->getMessage() ."\n");
			}
		}
		
		public static function editContent($post, $files, $type){
			try{
				$img = '';
				$date = date("F j");
				if (isset($files) && $files['previmg']['name']){
					$img = self::saveImg($files['previmg']);
					if ($img){
						$img = '/upload/'.$img;
					} else {
						$img = '/upload/placeholder.jpg';
					}
				}
				switch($type){
					case 'crypto':
							$sl_crypto = parent::$DBH->prepare("SELECT * FROM `vlog` WHERE `id`=?");
							$sl_crypto->execute([$post['id']]);
							$row_crypto = $sl_crypto->fetch();
							
							if (empty($img)) $img = $row_crypto['preview'];
							
							$up_crypto = parent::$DBH->prepare("UPDATE `vlog` SET `title`=?, `preview`=?, `body`=?, `view_count`=?, `type`=?, `create_at`=? WHERE `id`=?");
							$up_crypto->execute([$post['title'], $img, $post['text'], $post['view'], $post['type'], $post['date'], $post['id']]);
						break;
					case 'news':
							$sl_news = parent::$DBH->prepare("SELECT * FROM `posts` WHERE `id`=?");
							$sl_news->execute([$post['id']]);
							$row_news = $sl_news->fetch();
							
							if (empty($img)) $img = $row_news['preview'];
							
							$up_news = parent::$DBH->prepare("UPDATE `posts` SET `title`=?, `preview`=?, `body`=?, `view_count`=?, `project_id`=?,  `create_at`=? WHERE `id`=?");
							$up_news->execute([$post['title'], $img, $post['text'], $post['view'], $post['idProject'], $post['date'], $post['id']]);
						break;
					case 'project':
							$sl_project = parent::$DBH->prepare("SELECT * FROM `project` WHERE `id`=?");
							$sl_project->execute([$post['id']]);
							$row_project = $sl_project->fetch();
							
							if (empty($img)) $img = $row_project['preview'];
							
							$up_project = parent::$DBH->prepare("UPDATE `project` SET `name`=?, `preview`=?, `text`=?, `url`=?, `create_at`=?, `status`=?, `view`=? WHERE `id`=?");
							$up_project->execute([$post['title'], $img, $post['text'], $post['ref_link'], $post['date'],$post['status'], $post['view'], $post['id']]);
						break;
				}
			} catch (Exception $e){
				file_put_contents("error_log", $e);
				#parent::logError('Выброшено исключение: '.  $e->getMessage() ."\n");
			}
		}
		
		public static function addContent($post, $files, $type){
			try{
				$date = date("F j");
				$img = self::saveImg($files['previmg']);
				if ($img){
					$img = '/upload/'.$img;
				} else {
					$img = '/upload/placeholder.jpg';
				}
				switch($type){
					case 'crypto':
							$in_crypto = parent::$DBH->prepare("INSERT INTO `vlog`(`title`, `preview`, `body`, `view_count`, `type`, `create_at`) VALUES (?, ?, ?, ?, ?, ?)");
							$in_crypto->execute([$post['title'], $img, $post['text'], 0, $post['type'], $date]);
						break;
					case 'news':
							$in_news = parent::$DBH->prepare("INSERT INTO `posts`(`title`, `preview`, `body`, `create_at`, `project_id`) VALUES (?, ?, ?, ?,?)");
							$in_news->execute([$post['title'], $img, $post['text'], $date, $post['idProject']]);
						break;
					case 'project':
							$in_project = parent::$DBH->prepare("INSERT INTO `project`(`name`, `preview`, `text`, `url`, `status`, `create_at`) VALUES (?, ?, ?, ?, ?, ?)");
							$in_project->execute([$post['title'], $img, $post['text'], $post['ref_link'], $post['status'], $date]);
						break;
				}
			} catch (Exception $e){
				parent::logError('Выброшено исключение: '.  $e->getMessage() ."\n");
			}
		}
		
		
		public static function acceptInvest($post){
			try{
				$up_invest = parent::$DBH->prepare("UPDATE `invest` SET `status`='ok' WHERE `id`=?");
				$up_project = parent::$DBH->prepare("UPDATE `project` SET `invest_count`=`invest_count`+? WHERE `id`=?");
				$up_user = parent::$DBH->prepare("UPDATE `users` SET `investing_count`=`investing_count`+? WHERE `id`=?");
				
				$up_invest->execute([$post['invest_id']]);
				$up_project->execute([$post['amount'], $post['project_id']]);
				$up_user->execute([$post['amount'], $post['user_id']]);
				
				return $html;
			} catch (Exception $e){
				parent::logError('Выброшено исключение: '.  $e->getMessage() ."\n");
			}
		}
		
		public static function delInvest($post){
			try{
				$del_invest = parent::$DBH->prepare("UPDATE `invest` SET `status`='Отклонено' WHERE `id`=?");
				$del_invest->execute([$post['id']]);
				return $html;
			} catch (Exception $e){
				parent::logError('Выброшено исключение: '.  $e->getMessage() ."\n");
			}
		}
		
		public static function delNews($post){
			try{
				$del_news = parent::$DBH->prepare("DELETE FROM `posts` WHERE `id`=?");
				$del_news->execute([$post['id']]);
			} catch (Exception $e){
				parent::logError('Выброшено исключение: '.  $e->getMessage() ."\n");
			}
		}
		
		public static function delProject($post){
			try{
				$del_project = parent::$DBH->prepare("DELETE FROM `project` WHERE `id`=?");
				$del_project->execute([$post['id']]);
			} catch (Exception $e){
				parent::logError('Выброшено исключение: '.  $e->getMessage() ."\n");
			}
		}
		
		public static function delCrypto($post){
			try{
				$del_crypto = parent::$DBH->prepare("DELETE FROM `vlog` WHERE `id`=?");
				$del_crypto->execute([$post['id']]);
			} catch (Exception $e){
				parent::logError('Выброшено исключение: '.  $e->getMessage() ."\n");
			}
		}
		
		public static function delBanner($post){
			$dl_banner = parent::$DBH->prepare("DELETE FROM `banners` WHERE `id`=?");
			$dl_banner->execute([$post['id']]);
			return true;
		}
		public static function addBanners($post, $files){
			$time = $post['day'] * 24 * 60 * 60;
			$banners = self::saveImg($files['banners'], 'banner');
			$in_banners = parent::$DBH->prepare("INSERT INTO `banners`(`banner`, `url`, `time`) VALUES (?, ?, ?)");
			$in_banners->execute([$banners, $post['url'], time() + $time]);
			return true;
		}
		
		public static function registerUser($post){
			try{
				//проверка на валидную почту
				if (filter_var($post['email'], FILTER_VALIDATE_EMAIL)){
					if ($post['email'] && $post['password']){
						if (strlen($post['email']) <= 60){
							$sl_user = parent::$DBH->prepare("SELECT * FROM `users` WHERE `email`=?");
							$sl_user->execute([$post['email']]);
							$row_user = $sl_user->fetch();
							if (!$row_user['email']){
								if ($post['password'] != $post['confirm_password']) return 'Пароли не совпадают';
								$password = password_hash($post['Password'], PASSWORD_DEFAULT);
								$in_user = parent::$DBH->prepare("INSERT INTO `users`(`email`, `password`, `real_name`, `real_fname`, `investing_count`, `group`) VALUES (?, ?, ?, ?, ?, ?)");
								$in_user->execute([$post['email'], $password, 'Имя', 'Фамилия', '$0', 'users']);
								return 'Успешная регистрация';
							} else return 'Такой E-mail уже зарегистрирован';
						} else return 'Почта не более 60 символов';			
					} else return 'Заполните поля';
				} else return 'Введите почту';
				return false;
			} catch (Exception $e){
				parent::logError('Выброшено исключение: '.  $e->getMessage() ."\n");
			}
		}
		
	}