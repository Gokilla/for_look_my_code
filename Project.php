<?
	@session_start();
	
	class Project{
		public static $DBH;
		public static $user_id;
		
		public static function setup(){
			include("config.php");
			if ($_SESSION['admin'] == 'md5admin'){
				self::$DBH = new PDO($dsn, $user, $pass, $opt);
				if (isset($_SESSION['id']) && $_SESSION['id'])
					self::$user_id = $_SESSION['id'];
				return self::$DBH;
			} else {
				return false;
			}
		}
		
		public static function getIP(){
			$ip = 0;
			if (!empty($_SERVER['HTTP_CLIENT_IP'])){
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
			return $ip;
		}
		
		public static function getUserData($id = null){
			$sl_user = self::$DBH->prepare("SELECT * FROM `users` WHERE `id`=?");
			if ($id){
				$sl_user->execute([$id]);
			} else {
				$sl_user->execute([self::$user_id]);
			}
			return $sl_user->fetch();
		}
		
		public static function serializeArrayHtml($data){
			$arr = [];
			array_pop($data);
			foreach ($data as $key => $val){
				$arr[$key] = htmlspecialchars($val);
			}
			$arr['user_id'] = self::$user_id;
			return $arr;
		}
		
		public static function serializeArray($post){
			$array = [];
			foreach($post as $key => $val){
				$array[$key] = htmlspecialchars($val);
			}
			return $array;
		}
		
		public static function checkEmpty($data){
			$arr = [];
			if ($data){
				foreach ($data as $key => $val){
					$arr[$key] = ($val) ? $val : 'пусто';
				}
				return $arr;
			}
		}
		public static function logError($error = null){
			file_put_contents("error_log", $error);
		}
	}
	
	Project::setup();
	
?>